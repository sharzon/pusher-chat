/**
 * Created by sharzon on 18.01.17.
 */

(function () {
  var angApp = angular.module('sharzonChat', ['sharzonRoutes']);

  angApp.controller('MenuController', function ($scope) {

  });

  angApp.controller('MessagesController', function ($scope, $http) {
    // Get the messages of the current user
    $scope.getMessages = function () {
      $http({
        method: 'get',
        url: '/api/getMessages'
      }).then(function (response) {
        $scope.messages = response.data;
      }, function (response) {
        console.log(response.data, response.status);
      });

    };
  });
})();