(function () {
  var angApp = angular.module('sharzonRoutes', ['ngRoute']);

  angApp.config(['$routeProvider', '$locationProvider'],
    function ($routeProvider, $locationProvider) {
      $routeProvider
        .when('/messages', {
          templateUrl: 'views/messages.html',
          controller: 'MessagesController'
        })
    });
})();
