<!doctype html>
<html lang="en" ng-app="sharzonChat">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">

    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">

    <title>Sharzon Pusher Chat</title>
</head>
<body>

    <nav class="navbar navbar-default" ng-controller="MenuController">
        <div class="container-fluid">
            <div class="collapse navbar-collapse" id="main-menu">
                <ul class="nav navbar-nav">
                    <li><a href="/messages">Messages</a></li>
                    <li><a href="#">Users</a></li>
                    <li><a href="#">Profile</a></li>
                </ul>
            </div>
        </div>
    </nav>
    
    <div ng-view></div>

    <script src="https://cdnjs.cloudflare.com/ajax/libs/angular-ui-bootstrap/2.4.0/ui-bootstrap-tpls.min.js"></script>
    <script src="{{ asset('js/angular-modules/routes.js') }}"></script>
    <script src="https://ajax.googleapis.com/ajax/libs/angularjs/1.6.1/angular.min.js"></script>
    <script src="{{ asset('js/angular-app.js') }}"></script>
</body>
</html>