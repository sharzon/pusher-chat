<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class ChatsController extends Controller
{
    protected $user;

    public function __construct()
    {
        $this->middleware(function($request, $next) {
            $this->user = Auth::user();

            return $next($request);
        });
    }

    public function get()
    {
        if ($this->user)
        {
            return $this->user->chats;
        }

        return;
    }
}
