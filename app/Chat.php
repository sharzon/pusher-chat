<?php

namespace App;

use Illuminate\Support\Facades\Auth;
use Illuminate\Database\Eloquent\Model;

class Chat extends Model
{
    protected $fillable = [
        'first_user_id', 'second_user_id'
    ];

    public function getAnotherUserAttribute()
    {
        $user_id = Auth::id();

        // Return corresponding user
        if ($user_id == $this->first_user_id) {
            return;
        }

        if ($user_id == $this->second_user_id) {
            return;
        }

        return;
    }

    public function users()
    {
        return $this->belongsToMany('App\User', 'users_chats');
    }

    public function messages()
    {
        return $this->hasMany('App\Message');
    }
}
