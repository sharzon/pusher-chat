<?php

use App\User;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Model::unguard();

        $user = new User();
        $user->id = 1;
        $user->name = 'Ilia';
        $user->email = 'ilia@gmail.com';
        $user->password = Hash::make('ololo');
        $user->save();

        $user = new User();
        $user->id = 2;
        $user->name = 'Sharzon';
        $user->email = 'sharzon@gmail.com';
        $user->password = Hash::make('ololo');
        $user->save();

        $user = new User();
        $user->id = 3;
        $user->name = 'Someone';
        $user->email = 'someone@gmail.com';
        $user->password = Hash::make('ololo');
        $user->save();

        $user = new User();
        $user->id = 4;
        $user->name = 'Borshch';
        $user->email = 'Borshch@gmail.com';
        $user->password = Hash::make('ololo');
        $user->save();
    }
}
