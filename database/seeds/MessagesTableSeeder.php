<?php

use App\Message;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Seeder;

class MessagesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $message = new Message();
        $message->chat_id = 1;
        $message->user_id = 2;
        $message->text = "First message";
        $message->save();

        $message = new Message();
        $message->chat_id = 1;
        $message->user_id = 1;
        $message->text = "It's something";
        $message->save();

        $message = new Message();
        $message->chat_id = 2;
        $message->user_id = 2;
        $message->text = "Something's going wrong. Or not?";
        $message->save();

        $message = new Message();
        $message->chat_id = 2;
        $message->user_id = 2;
        $message->text = "Help me!";
        $message->save();

        $message = new Message();
        $message->chat_id = 2;
        $message->user_id = 3;
        $message->text = "I code for food.";
        $message->save();

        $message = new Message();
        $message->chat_id = 4;
        $message->user_id = 3;
        $message->text = "Strange";
        $message->save();
    }
}
