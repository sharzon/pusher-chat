<?php

use App\Chat;
//use App\User;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Seeder;

class ChatsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Model::unguard();

        $chat = new Chat();
        $chat->id = 1;
//        $chat->first_user_id = 1;
//        $chat->second_user_id = 2;
        $chat->save();
        $chat->users()->attach([1, 2]);

        $chat = new Chat();
        $chat->id = 2;
//        $chat->first_user_id = 2;
//        $chat->second_user_id = 3;
        $chat->save();
        $chat->users()->attach([2, 3]);

        $chat = new Chat();
        $chat->id = 3;
//        $chat->first_user_id = 4;
//        $chat->second_user_id = 3;
        $chat->save();
        $chat->users()->attach([3, 4]);

        $chat = new Chat();
        $chat->id = 4;
//        $chat->first_user_id = 1;
//        $chat->second_user_id = 3;
        $chat->save();
        $chat->users()->attach([1, 3]);
    }
}
